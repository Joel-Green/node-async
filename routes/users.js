var express = require('express');
var router = express.Router();

var User = require('../models/user');

router.get('/', async (req, res) => {
  try {
    const users = await User.find().exec();
    res.json({  data: users, success: true,});
  }
  catch (err) {
    console.log(err);
  }
})

router.get('/:id', async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.json({data:user, success:true});
  }
  catch (err) {
    console.log(err);
  }
})

router.post('/', async (req, res) => {
  try {
    const user = await new User({
      name: req.body.name,
      email: req.body.email
    }).save();

    res.json({ message: 'User Added', success: true });
  }
  catch (err) {
    console.log(err);
  }
})

router.delete('/:id', async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id).exec();
    res.json({ message: 'User Deleted', success: true });
  }
  catch (err) {
    console.log(err);
  }
})

router.put('/:id', async (req, res) => {
  try {
    let updateObj = req.body;
    const user = await User.findByIdAndUpdate(req.params.id, updateObj).exec();
    res.json({ message: 'User Updated', success: true });
  }
  catch (err) {
    console.log(err);
  }
})

module.exports = router;
